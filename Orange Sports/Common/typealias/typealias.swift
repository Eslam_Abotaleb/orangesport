//
//  typealias.swift
//  Orange Sports
//
//  Created by Islam Abotaleb on 8/13/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import Foundation
//network use generic
typealias completionHandlerForExecute<Request: RequestServiceProtocol> =
    (NetworkServiceResult<Request.ResponseModel, Request.ResponseError>)->()
