//
//  BaseViewController.swift
//  Orange Sports
//
//  Created by Islam Abotaleb on 8/13/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit
import Toast_Swift
class BaseViewController: UIViewController {

    func showErrorView(errorTitle: String, errorDescription: String) {
        self.view.endEditing(true)
        var style = ToastStyle()
        style.backgroundColor = AppColors.blackColor
        style.messageColor = UIColor(red: 255, green: 102, blue: 0, alpha: 1.0)
        style.messageAlignment = .right
        style.messageFont = UIFont.textStyle
        self.view.makeToast(errorDescription, duration: 5.0, position: .bottom, style: style)
    }

}
