//
//  Urls.swift
//  Orange Sports
//
//  Created by Islam Abotaleb on 8/13/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import Foundation

enum Urls {
    case empty
    case baseUrl
    case subscribtionBaseUrl
    var baseUrl: String {
        switch self {
        case .empty:
            return ""
        case .baseUrl:
            return ""
        case .subscribtionBaseUrl:
            return ""
        }
    }
    enum Path {
         case empty
       var absolutePath: String {
           switch self {
           case .empty:
            return ""

            }
       }
    }
    
    enum HttpMethod: String {
        case get = "GET"
        case post = "POST"
    }
    
    enum Scheme: String {
        case https
    }
}
