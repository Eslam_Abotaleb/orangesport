//
//  Keys.swift
//  Orange Sports
//
//  Created by Islam Abotaleb on 8/13/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//


import UIKit

struct Keys {
    static let userObject = "userObject"
    static let userType = "userType"
    static let token = "token"
    static let msisdn = "msisdn"
    static let logged = "logged"
    
    static let language = "language"
    static let hideKeyboard = "hideKeyboard"
    
    static var apiKey = ""
    static var subscribeApiKey = ""
   
    
    
    static let portalId = "321"
    static let homeSlideshowID = "323"
    static let homeNewsID = "372"
    static let homeStudioEtisalatVideosID = "347"
    static let homeHadaryVideosID = "374"
    static let homeEtisalatLocalVideosID = "372"
    
  
}


