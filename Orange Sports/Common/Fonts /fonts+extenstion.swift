//
//  fonts+extenstion.swift
//  Orange Sports
//
//  Created by Islam Abotaleb on 8/13/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit
extension UIFont {
    
    class var textStyle: UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Roman", size: 11.0)!
    }
    class var textStyle3: UIFont {
        return UIFont(name: "HelveticaNeue-Medium-", size: 16.0)!
    }
    
    class var textStyle2: UIFont {
        return UIFont(name: "HelveticaNeueLTArabic-Roman", size: 12.0)!
    }
  
}
